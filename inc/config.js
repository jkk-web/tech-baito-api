// 定数で管理
// TODO: .envとかにしまう
const pg = require("pg");

exports.DB_NAME = "postgres";
exports.DB_USER = "postgres";
exports.DB_PASSWORD = "postgres";

// TODO: タイミング 逃したけど、いつか切り替えたい
exports.DB_POOL = pg.Pool({
    database: "postgres",
    user: "postgres",
    password: "postgres",
    host: "localhost",
    port: 5432
});
