const express = require("express");
let app = express();
const pg = require("pg");
const bodyParser = require("body-parser");
const config = require(__dirname + "/inc/config");

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

const initDatabase = function(mainApp) {
  const pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  var tables = [];

  var promise = new Promise((resolve, reject) => {
    // table の確認
    pool.connect(function(err, client) {
      const query =
        "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE'";
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
        } else {
          result.rows.forEach(element => {
            tables.push(element.table_name);
          });
          console.log("created tables : ");
          resolve("done");
        }
      });
    });
  })

  // この promise は、table をすべて削除したいときにコメントアウトを外してください。
/*
   .then((msg)=> {
     return new Promise((resolve, reject) => {
       pool.connect(function(err, client) {
         const tablesQuery = tables.join(", ");
         const query = "DROP TABLE IF EXISTS " + tablesQuery + ";";
         client.query(query, function(err, result) {
           if (err) {
             console.log(err);
           } else {
             console.log("finish delete all table.");
             tables = [];
             resolve("done");
           }
         });
       });
     });
   })
*/
  .then((msg) => {
    // Skills table add
    // TODO: 初期データ追加
    console.log(tables);
    if (tables.indexOf("skills") === -1) {
      pool.connect(function(err, client) {
        const query = `
        CREATE TABLE Skills(
          recruit_id integer NOT NULL,
          skill_no integer NOT NULL,
          language varchar(32) NOT NULL,
          framework varchar(32) NOT NULL,
          level varchar(1) NOT NULL
        );
        `;

          client.query(query, function(err, result) {
            if (err) {
              if (err.code === "42P07") {
                // table 重複の error code
                //pass
              } else {
                console.log(err);
              }
            } else {
              console.log('make table "skills"');
            }
          });
        });
      }

      if (tables.indexOf("tags") === -1) {
        pool.connect(function(err, client) {
          const query = `
        CREATE TABLE Tags(
          recruit_id integer NOT NULL,
          tag_no integer NOT NULL,
          content varchar(32) NOT NULL
        );
        `;

          client.query(query, function(err, result) {
            if (err) {
              if (err.code === "42P07") {
                // table 重複の error code
                //pass
              } else {
                console.log(err);
              }
            } else {
              console.log('make table "tags"');
            }
          });
        });
      }

      // user テーブル
      if (tables.indexOf("users") === -1) {
        pool.connect(function(err, client) {
          const query = `
        CREATE TABLE Users(
          user_id varchar(255) NOT NULL,
          name varchar(255),
          mail varchar(255) NOT NULL,
          occupation varchar(255) NOT NULL,
          prefecture varchar(32),
          city varchar(32),
          station varchar(32),
          portfolio varchar(32),
          pr varchar(255)
        );
        `;
          const query2 = `
          INSERT INTO Users VALUES (
            't9JvAlw5xbTXuyFiCfd9AQCI8Q33',
            'ほげほげ 太郎',
            'hogehoge_tarou@gmail.com',
            'student',
            '東京都',
            '調布市',
            '調布駅',
            'http://hogehoge.com',
            'ほげほげです。がんばります。'
          );
        `;
          client.query(query + query2, function(err, result) {
            if (err) {
              if (err.code === "42P07") {
                // table 重複の error code
                //pass
              } else {
                console.log(err);
              }
            } else {
              console.log('make table "users"');
            }
          });
        });
      }

      // 会社テーブル
      if (tables.indexOf("companies") === -1) {
        pool.connect(function(err, client) {
          const query = `
        CREATE TABLE Companies(
          co_id varchar(32) NOT NULL,
          state varchar(32) NOT NULL,
          co_name varchar(255) NOT NULL,
          department varchar(255) NOT NULL,
          representative varchar(255) NOT NULL,
          mail varchar(255) NOT NULL,
          tel varchar(32) NOT NULL,
          fax varchar(32),
          postal varchar(255) NOT NULL,
          prefecture varchar(32) NOT NULL,
          city varchar(32) NOT NULL,
          address varchar(255) NOT NULL,
          station varchar(32),
          url varchar(255)
        );
        `;
          client.query(query, function(err, result) {
            if (err) {
              if (err.code === "42P07") {
                // table 重複の error code
                //pass
              } else {
                console.log(err);
              }
            } else {
              console.log('make table "companies"');
            }
          });
        });
      }

      // 求人テーブル
      if (tables.indexOf("recruitments") === -1) {
        pool.connect(function(err, client) {
          const query = `
        CREATE TABLE Recruitments(
          recruit_id SERIAL PRIMARY KEY,
          co_id varchar(255) NOT NULL,
          title varchar(255) NOT NULL,
          start_date TIMESTAMP WITH TIME ZONE NOT NULL,
          finish_date TIMESTAMP WITH TIME ZONE NOT NULL,
          explain varchar(255) NOT NULL,
          picture varchar(255),
          postal varchar(255),
          prefecture varchar(255),
          city varchar(255),
          address varchar(255),
          station varchar(32),
          wage integer NOT NULL,
          day_num integer NOT NULL,
          remote varchar(32) NOT NULL
        );
        `;
          /**
           * 初期データ: このrequest bodyでひっかかる
           * {"prefecture":"tokyo","city":"sinjuku","minWage":700,"maxWage":1100,"minSift":0,"maxSift":3,"remote":"either"} */

          const query2 = `
        INSERT INTO Recruitments (
          co_id,
          title,
          start_date,
          finish_date,
          explain,
          prefecture,
          city,
          wage,
          day_num,
          remote
        ) VALUES (
          't9JvAlw5xbTXuyFiCfd9AQCI8Q33',
          'init 求人',
          '2020-03-05 09:00:00+09',
          '2020-04-01 09:00:00+09',
          'init 説明',
          'tokyo',
          'sinjuku',
          800,
          2,
          '1'
        );
        `;
          client.query(query + query2, function(err, result) {
            if (err) {
              if (err.code === "42P07") {
                // table 重複の error code
                //pass
              } else {
                console.log(err);
              }
            } else {
              console.log('make table "recruitments"');
            }
          });
        });
      }

      if (tables.indexOf("nodejsdata") === -1) {
        pool.connect(function(err, client) {
          const query = `
          CREATE TABLE nodejsdata(
            user_id varchar(32) NOT NULL,
            data1 varchar(255),
            data2 varchar(255),
            data3 varchar(32),
            data4 varchar(32),
            data5 varchar(32));
        `;
          client.query(query, function(err, result) {
            if (err) {
              if (err.code === "42P07") {
                // table 重複の error code
                //pass
              } else {
                console.log(err);
              }
            } else {
              console.log('make table "nodejsdata"');
            }
          });
        });
      }

      // 応募テーブル
      if (tables.indexOf("applications") === -1) {
        pool.connect(function(err, client) {
          const query = `
        CREATE TABLE applications(
          apply_id SERIAL PRIMARY KEY,
          user_id VARCHAR(255),
          recruit_id INTEGER,
          state INTEGER
        );`;
          // 初期データ入れたいとき
          const query2 = `
          INSERT INTO applications (
            recruit_id, user_id, state
          ) VALUES
          (1, 't9JvAlw5xbTXuyFiCfd9AQCI8Q33', 0),
          (1, 't9JvAlw5xbTXuyFiCfd9AQCI8Q33', 1),
          (1, 't9JvAlw5xbTXuyFiCfd9AQCI8Q33', -1),
          (1, 't9JvAlw5xbTXuyFiCfd9AQCI8Q33', -2),
          (1, 't9JvAlw5xbTXuyFiCfd9AQCI8Q33', 2),
          (1, 't9JvAlw5xbTXuyFiCfd9AQCI8Q33', 0)
          ;
        `;
          client.query(query + query2, function(err, result) {
            if (err) {
              if (err.code === "42P07") {
                // table 重複の error code
                //pass
              } else {
                console.log(err);
              }
            } else {
              console.log('make table "applications"');
            }
          });
        });
      }
    });
};

exports.initDB = initDatabase;
