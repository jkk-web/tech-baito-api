const express = require("express");
const pg = require("pg");
const app = express();
const config = require("../inc/config");

app.get("/api/company/get_all", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      client.query("SELECT name FROM Companies", function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.get("/api/company/find", function(req, res, next) {
  console.log("会社情報を取得します");
  console.log(req.body);
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "SELECT * FROM Companies WHERE co_id = $1;",
        values: [req.query.co_id]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result.rows);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

/*
  insert **post
  arguments --
        co_id varchar(32) NOT NULL,
        state varchar(32) NOT NULL,
        co_name varchar(255) NOT NULL,
        department varchar(255),
        representative varchar(255) NOT NULL,
        mail varchar(255) NOT NULL,
        tel varchar(32) NOT NULL,
        fax varchar(32),
        postal varchar(255) NOT NULL,
        prefecture varchar(32) NOT NULL,
        city varchar(32) NOT NULL,
        address varchar(255) NOT NULL,
        url varchar(255)
  return --
  */

app.post("/api/company/insert", function(req, res, next) {
  console.log("企業登録するよ");
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      if(req.body.station === "選択されていません")
        req.body.station = null;
      const query = {
        text:
          "INSERT INTO Companies VALUES ($1, $2 ,$3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14);",
        values: [
          req.body.co_id,
          "0",
          req.body.name,
          req.body.department,
          req.body.representative,
          req.body.email,
          req.body.tel,
          req.body.fax,
          req.body.postal,
          req.body.prefecture,
          req.body.city,
          req.body.address,
          req.body.station,
          req.body.url
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

/*
  insert test **post
  arguments --
    string id,
    string name,
    string mail,
    string prefecture,
  return --
  */

app.post("/api/company/update", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text:
          "UPDATE Companies SET user_id=$1, name=$2, mail=$3, prefecture=$4 WHERE user_id=$1;",
        values: [
          req.body.userId,
          req.body.name,
          req.body.mail,
          req.body.prefecture
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});
/*
  delete test **post
  arguments --
    string id

  return --
  */

app.post("/api/company/delete", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "DELETE FROM Users WHERE id=$1",
        values: [req.body.id]
      };

      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          if (result.rowCount > 0) {
            res.json({
              data: "",
              result: true
            });
          } else {
            res.json({
              result: false
            });
          }
        }
      });
    }
  });
});

  app.post('/api/test/upload', function(req, res, next) {

  });

exports.companiesApp = app;
