const express = require("express");
const pg = require("pg");
const app = express();
const config = require("../inc/config");

/**
 *  query 関係の templete です。
 *  template ってなってるとこを修正してください。
 */

app.get("/api/templete/get_all", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      client.query("SELECT * FROM templete", function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

exports.templeteApp = app;
