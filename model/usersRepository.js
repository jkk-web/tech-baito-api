const express = require("express");
const pg = require("pg");
const app = express();
const config = require("../inc/config");

/*
arguments --
return --
  array name
*/

app.get("/api/user/get_all", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      client.query("SELECT name FROM Users", function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.get("/api/user/find", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "SELECT * FROM Users WHERE user_id = $1;",
        values: [req.query.user_id]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

/*
  insert test **post
  arguments --
    string user_id,
    string name,
    string mail,
    string ocupation,
    string prefecture,
    string city,
    string station
  return --
  */

app.post("/api/user/insert", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "INSERT INTO Users VALUES ($1, $2 ,$3, $4);",
        values: [req.body.user_id, "", req.body.mail, req.body.occupation]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.post("/api/user/update", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text:
          "UPDATE Users SET user_id=$1, name=$2, occupation=$3, prefecture=$4, city=$5, station=$6, portfolio=$7, pr=$8 WHERE user_id=$1;",
        values: [
          req.body.userId,
          req.body.name,
          req.body.occupation,
          req.body.prefecture,
          req.body.city,
          req.body.station,
          req.body.portfolio,
          req.body.pr
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});
/*
  delete test **post
  arguments --
    string id

  return --
  */

app.post("/api/user/delete", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "DELETE FROM Users WHERE id=$1",
        values: [req.body.id]
      };

      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          if (result.rowCount > 0) {
            res.json({
              data: "",
              result: true
            });
          } else {
            res.json({
              result: false
            });
          }
        }
      });
    }
  });
});

exports.userApp = app;
