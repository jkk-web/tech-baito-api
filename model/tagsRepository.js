const express = require("express");
const pg = require("pg");
const app = express();
const config = require("../inc/config");

/**
 *  query 関係の templete です。
 *  template ってなってるとこを修正してください。
 */

app.get("/api/tag/get_all", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      client.query("SELECT * FROM Tags", function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.get("/api/tag/find", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "SELECT * FROM Tags WHERE recruit_id = $1;",
        values: [req.query.recruit_id]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

/*
  insert test **post
  arguments --
    string tag_id,
    string name,
    string mail,
    string ocupation,
    string prefecture,
    string city,
    string station
  return --
  */

app.post("/api/tag/insert", function(req, res, next) {
  console.log("タグ登録するよ");
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      let array = DataToArray(req.body);
      console.log(array);
      let query = "INSERT INTO Tags VALUES (" + req.body.recruit_id + "," + "0"+ "," + array[0] +")";
      for(let i=1; i < array.length; i++){
        console.log(array[i]);
        query  += ", " + "(" + req.body.recruit_id + "," + i + "," + array[i] + ")";
      }
      query += ";";
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
      console.log("タグを登録しました");
    }
  });
});

//未編集．要改善
app.post("/api/tag/update", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text:
          "UPDATE Tags SET recruit_id=$1, name=$2, mail=$3, prefecture=$4 WHERE recruit_id=$1;",
        values: [
          req.body.userId,
          req.body.name,
          req.body.mail,
          req.body.prefecture
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});
/*
  delete test **post
  arguments --
    string id

  return --
  */

//未編集．要改善
app.post("/api/tag/delete", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "DELETE FROM Users WHERE id=$1",
        values: [req.body.id]
      };

      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          if (result.rowCount > 0) {
            res.json({
              data: "",
              result: true
            });
          } else {
            res.json({
              result: false
            });
          }
        }
      });
    }
  });
});

const DataToArray = (body) =>{
  let array = [];
  console.log(body);
  for(let i=0;i < body.tags.length; i++)
    if(body.tags[i][0] != '')
      array.push(body.tags[i][0]);
  return array;
};

exports.tagsApp = app;
