const express = require("express");
const pg = require("pg");
const app = express();
const config = require("../inc/config");

app.get("/api/skills/get_all", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      client.query("SELECT * FROM Skills", function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.post("/api/skill/insert", function(req, res, next) {
  console.log("スキル追加するよ");
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      let array = DataToArray(req.body);
      let query = "INSERT INTO Skills VALUES (" + req.body.recruit_id + "," + "0" + "," + array[0][0] + "," + array[0][1] + "," + array[0][2] + ")";
      for(let i=1; i < array.length; i++){
        query  += ", " + "(" + req.body.recruit_id + "," + i + "," + array[i][0] + "," + array[i][1] + "," + array[i][2] + ")";
      }
      query += ";";
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

const DataToArray = (body) =>{
  let array = [];
  for(let i=0;i < body.skills.length; i++)
    if(body.skills[i][0] != '')
      array.push(body.skills[i]);
  return array;
};

exports.skillsApp = app;
