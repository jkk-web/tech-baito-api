const express = require("express");
const pg = require("pg");
const app = express();
const config = require("../inc/config");

// 全データ取得
app.get("/api/applications/get_all", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      client.query("SELECT * FROM applications", function(err, result) {
        if (err) {
          console.log(err);
          res.json({ result: false });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

// user id に該当するデータを取得
app.get("/api/applications/find", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    }
    let query = {
      // ユーザIDでフィルター、応募IDの降順にソートしたやつを返す i.e. 新しいものが上になるように
      // text: `SELECT * FROM Applications WHERE user_id = $1 ORDER BY apply_id DESC;`,
      text: `SELECT
              companies.co_name,
              applications.state,
              applications.apply_id
            FROM 
              applications
            INNER JOIN recruitments ON applications.recruit_id = recruitments.recruit_id
            INNER JOIN companies ON recruitments.co_id = companies.co_id
            WHERE applications.user_id = $1
            ORDER BY applications.apply_id DESC;
            `,
      values: [req.query.user_id]
    };
    client.query(query, function(err, result) {
      if (err) {
        console.log(err);
        res.json({
          result: false
        });
      } else {
        res.status(200).json(result.rows);
      }
    });
  });
});

// recruit id に該当するデータを取得
app.post("/api/applications/get_users", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      let query = {
        text:
          "SELECT *  FROM Applications " +
          "INNER JOIN Users ON Users.user_id = Applications.user_id " +
          "WHERE recruit_id = $1 " +
          "AND state = $2 ",
        values: [
          req.body.recruit_id,
          req.body.state,
        ]
      };
      query.text += "ORDER BY applications.apply_id DESC"; //とりあえず掲載の新しい順で表示．あとで選択できるようにする．

      query.text += ";";
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result.rows);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

// 求人に応募しているユーザ数をカウント
app.get("/api/applications/count", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    }
    let query = {
      text: `SELECT COUNT(*) FROM Applications WHERE recruit_id = $1;`,
      values: [req.query.recruit_id]
    };
    client.query(query, function(err, result) {
      if (err) {
        console.log(err);
        res.json({
          result: false
        });
      } else {
        res.status(200).json(result.rows);
      }
    });
  });
});

// 新しいデータを1行分挿入
app.post("/api/applications/insert", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    }
    let query = {
      text: `INSERT INTO applications (recruit_id, user_id, state) VALUES ($1, $2, $3) RETURNING apply_id; 
      `,
      values: [req.body.recruit_id, req.body.user_id, req.body.state]
    };
    client.query(query, function(err, result) {
      if (err) {
        console.log(err);
        res.json({
          result: false
        });
      } else {
        res.status(200).json(result.rows[0]);
      }
    });
  });
});

app.post("/api/applications/judge", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    }
    let query = {
      text: `UPDATE Applications SET state = $1 WHERE recruit_id = $2 AND user_id = $3; 
      `,
      values: [req.body.state, req.body.recruit_id, req.body.user_id]
    };
    client.query(query, function(err, result) {
      if (err) {
        console.log(err);
        res.json({
          result: false
        });
      } else {
        console.log(result);
        res.json({
          data: result.rows,
          result: true
        });
      }
    });
  });
});

exports.applicationsApp = app;
