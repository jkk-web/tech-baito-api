const express = require("express");
const pg = require("pg");
const app = express();
const config = require("../inc/config");

app.get("/api/recruitment/get_all", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      client.query("SELECT * FROM Recruitments", function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

//テンプレ
app.post("/api/recruitment/find", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "SELECT FROM Recruitments WHERE recruit_id = $1;",
        values: [req.query.userId]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.get("/api/recruitment/collect", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "SELECT * FROM Recruitments WHERE co_id = $1;",
        values: [req.query.companyID]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          res.status(200).json(result.rows);
        }
      });
    }
  });
});

//ユーザの指定通りに検索し，ヒットした情報を全て返す
app.post("/api/recruitment/get_user_required", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  //"INNER JOIN ( SELECT recruit_id, STRING_AGG (language, ',') , STRING_AGG (framework, ','), STRING_AGG (level, ',') " +
  //                      "FROM Skills GROUP BY recruit_id ) AS NewSkills ON NewSkills.recruit_id = Recruitments.recruit_id " +

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      let query = {
        text:
          "SELECT *  FROM Recruitments " +
          "INNER JOIN Companies ON Companies.co_id = Recruitments.co_id " +
          "INNER JOIN ( SELECT recruit_id, STRING_AGG (content, ',') AS content FROM Tags GROUP BY recruit_id ) AS NewTags " +
                        "ON NewTags.recruit_id = Recruitments.recruit_id " +
          "INNER JOIN ( SELECT recruit_id, STRING_AGG (language, ',') AS language, " +
                        "STRING_AGG (framework, ',') AS framework, STRING_AGG (level, ',') AS level " +
                        "FROM Skills GROUP BY recruit_id ) AS NewSkills ON NewSkills.recruit_id = Recruitments.recruit_id " +
          "WHERE (start_date <= NOW() AND NOW() <= finish_date) " +
          "AND ($1 <= wage AND wage <= $2) " +
          "AND ($3 <= day_num AND day_num <= $4) ",
        values: [
          req.body.minWage,
          req.body.maxWage,
          req.body.minSift,
          req.body.maxSift
        ]
      };
      if (req.body.remote != "either") {
        query.text += "AND  $5 = remote ";
        query.values.push(req.body.remote);
      }
      //query.text += "UNION SELECT STRING_AGG ( content, ',') FROM Tags GROUP BY recruit_id WHERE Tags.recruit_id = Recruitments.recruit_id";
      //query.text += "ORDER BY start_date DESC"; //とりあえず掲載の新しい順で表示．あとで選択できるようにする．

      query.text += ";";
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result.rows);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.post("/api/recruitment/insert", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    }
    let query = {
      text: `INSERT INTO Recruitments (
          co_id, title, start_date, finish_date, explain, picture, postal, prefecture, city, address, wage, day_num, remote) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) RETURNING recruit_id;`,

      values: [
        req.body.coId,
        req.body.title,
        req.body.startDate,
        req.body.finishDate,
        req.body.explain,
        req.body.picture,
        req.body.postal,
        req.body.prefecture,
        req.body.city,
        req.body.address,
        req.body.wage,
        req.body.dayNum,
        req.body.remote,
      ]
    };
    client.query(query, function(err, result) {
      if (err) {
        console.log(err);
        res.json({
          result: false
        });
      } else {
        console.log("insert into recruitments");
        res.status(200).json(result.rows[0]); // { recruit_id: n } を返す
      }
    });
  });
});

/*テンプレのままで求人テーブルが対応していない(要変更) */
app.post("/api/recruitment/update", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text:
          "UPDATE Users SET user_id=$1, name=$2, mail=$3, prefecture=$4 WHERE user_id=$1;",
        values: [
          req.body.userId,
          req.body.name,
          req.body.mail,
          req.body.prefecture
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          console.log(result);
          res.json({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
});

app.post("/api/recruitment/delete", function(req, res, next) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });
  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "DELETE FROM Users WHERE id=$1",
        values: [req.body.id]
      };

      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          res.json({
            result: false
          });
        } else {
          if (result.rowCount > 0) {
            res.json({
              data: "",
              result: true
            });
          } else {
            res.json({
              result: false
            });
          }
        }
      });
    }
  });
});

exports.recruitmentsApp = app;
