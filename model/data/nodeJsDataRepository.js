const pg = require("pg");
const config = require("../../inc/config");

/**
 * findNodeJsData
 * @param {string} userId;
 */

const findNodeJsData = function(userId) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "SELECT FROM nodejsdata userId=$1;",
        values: [userId]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
};

/**
 * insertNodoJsData
 * @param {string} userId
 * @param {object} data
 */

const insertNodeJsData = function(userId, data) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "INSERT INTO nodejsdata VALUES ($1, $2, $3, $4, $5, $6);",
        values: [
          userId,
          data.data1,
          data.data2,
          data.data3,
          data.data4,
          data.data5
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            result: true
          });
        }
      });
    }
  });
};

/**
 * updataNodeJsData
 * @param {string} userId
 * @param {object} data
 */

const updateNodeJsData = function(userId, data) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text:
          "UPDATE nodejsdata SET data1=$1, data2=$2, data3=$3, data4=$4, data5=$5 WHERE user_id = $6;",
        values: [
          data.data1,
          data.data2,
          data.data3,
          data.data4,
          data.data5,
          userId
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            result: true
          });
        }
      });
    }
  });
};

/**
 * deleteNodeJsData
 * @param {string} userId
 */

const deleteNodeJsData = function(userId) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "DELETE FROM nodejsdata WHERE user_id = $1;",
        values: [userId]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            result: true
          });
        }
      });
    }
  });
};

exports.findNodeJsData = findNodeJsData;
exports.insertNodeJsData = insertNodeJsData;
exports.updateNodeJsData = updateNodeJsData;
exports.deleteNodeJsData = deleteNodeJsData;
