const pg = require("pg");
const config = require("../../inc/config");

// Template ??????????????????

/**
 * findTemplateData
 * @param {string} userId;
 */

const findTemplateData = function(userId) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "SELECT FROM Templatedata userId=$1;",
        values: [userId]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            data: result.rows,
            result: true
          });
        }
      });
    }
  });
};

/**
 * insertNodoJsData
 * @param {string} userId
 * @param {object} data
 */

const insertTemplateData = function(userId, data) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "INSERT INTO Templatedata VALUES ($1, $2, $3, $4, $5, $6);",
        values: [
          userId,
          data.data1,
          data.data2,
          data.data3,
          data.data4,
          data.data5
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            result: true
          });
        }
      });
    }
  });
};

/**
 * updataTemplateData
 * @param {string} userId
 * @param {object} data
 */

const updateTemplateData = function(userId, data) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text:
          "UPDATE Templatedata SET data1=$1, data2=$2, data3=$3, data4=$4, data5=$5 WHERE user_id = $6;",
        values: [
          data.data1,
          data.data2,
          data.data3,
          data.data4,
          data.data5,
          userId
        ]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            result: true
          });
        }
      });
    }
  });
};

/**
 * deleteTemplateData
 * @param {string} userId
 */

const deleteTemplateData = function(userId) {
  var pool = pg.Pool({
    database: config.DB_NAME,
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    host: "localhost",
    port: 5432
  });

  pool.connect(function(err, client) {
    if (err) {
      console.log(err);
    } else {
      const query = {
        text: "DELETE FROM Templatedata WHERE user_id = $1;",
        values: [userId]
      };
      client.query(query, function(err, result) {
        if (err) {
          console.log(err);
          return JSON.stringify({
            result: false
          });
        } else {
          return JSON.stringify({
            result: true
          });
        }
      });
    }
  });
};

exports.findTemplateData = findTemplateData;
exports.insertTemplateData = insertTemplateData;
exports.updateTemplateData = updateTemplateData;
exports.deleteTemplateData = deleteTemplateData;
