const express = require("express")
const app = express()

/** ここで描くデータのリポジトリを呼び出す */

const nodejsDataRep = require('./data/nodeJsDataRepository');
//const templateDataRep = require('./data/templateDataRepository');


/** Data update --post
 * -- arguments --
 * subject
*/

app.post('/api/data/find', function(req, res, next) {

  let subject = req.body.subject

  // ここで、subject として nodeJs やら, ruby やらを switch 文の判定として利用する。

  switch(subject){
    case 'nodeJs' :
      res.json({
        "data": JSON.parse(nodejsDataRep.findNodeJsData(subject)),
        "result": true,
      });
      break;

  }
});

/** Data insert --post
 * -- arguments --
 * subject
*/

app.post('/api/data/insert', function(req, res, next) {

  let subject = req.body.subject
  let userId = req.body.userId
  let data = req.body.data

    // ここで、subject として nodeJs やら, ruby やらを switch 文の判定として利用する。

  switch(subject){
    case 'nodeJs' :
      res.json({
        "data": nodejsDataRep.insertNodeJsData(userId, data),
        "result": true,
      });
        break;

    }
  });

/** Data update --post
 * -- arguments --
 * subject
*/

app.post('/api/data/update', function(req, res, next) {

  let subject = req.body.subject
  let data = req.body.data

    // ここで、subject として nodeJs やら, ruby やらを switch 文の判定として利用する。

  switch(subject){
    case 'nodeJs' :
      var result = nodejsDataRep.updateNodeJsData(subject, data);
      res.json({
        "state": JSON.parse(result).result,
        "result": true
      });
  }
});

/** Data update --post
 * -- arguments --
 * subject
*/


app.post('/api/data/delete', function(req, res, next) {

  let subject = req.body.subject
  let result = {};

    // ここで、subject として nodeJs やら, ruby やらを switch 文の判定として利用する。

  switch(subject){
    case 'nodeJs' :
      result = nodejsDataRep.deleteNodeJsData(subject);
    }

  return result;
});

exports.dataApp = app;
