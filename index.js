const express = require("express");
let app = express();
const bodyParser = require("body-parser");

const init = require("./initDB");

// CROS 対策
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Max-Age", "86400");
  next();
});
app.options("*", (req, res) => {
  res.sendStatus(200);
});


app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());

// stack 内の layer の追記
// TODO いつかいい感じに変える
// さすがに _router 見るのはヤバい
function mergeStack(mainApp, setApp) {
  const stack = setApp._router.stack;
  stack.forEach(layer => {
    if (layer.name == "bound dispatch") {
      mainApp._router.stack.push(layer);
    }
  });
  return mainApp;
}

// dataReporitory の呼び出し
const dataRep = require("./model/dataController");
if (dataRep) {
  app = mergeStack(app, dataRep.dataApp);
}

// usersRepository の呼び出し
const usersRep = require("./model/usersRepository");
if (usersRep) {
  app = mergeStack(app, usersRep.userApp);
}

// skillsRepository の呼び出し
const skillsRep = require("./model/skillsRepository");
if (skillsRep) {
  app = mergeStack(app, skillsRep.skillsApp);
}

// tagsRepository の呼び出し
const tagsRep = require("./model/tagsRepository");
if (tagsRep) {
  app = mergeStack(app, tagsRep.tagsApp);
}

// companiesRepository の呼び出し
const companiesRep = require("./model/companiesRepository");
if (companiesRep) {
  app = mergeStack(app, companiesRep.companiesApp);
}

// recruitmentsRepository の呼び出し
const recruitmentsRep = require("./model/recruitmentsRepository");
if (recruitmentsRep) {
  app = mergeStack(app, recruitmentsRep.recruitmentsApp);
}

// applicationRepositoryの呼び出し
const applicationsRep = require("./model/applicationsRepository");
if (applicationsRep) {
  app = mergeStack(app, applicationsRep.applicationsApp);
}

// templateRepository の呼び出し

// const templateRep = require('./model/templateRepository');
// if(templateRep){
//   app = mergeStack(app, templateRep.userApp);
// }

// Server の起動
var server = app.listen(4000, function() {
  init.initDB();
  console.log("Listen port : " + server.address().port);
});
